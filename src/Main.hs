module Main where

import Control.Arrow
import Data.Maybe
import Control.Monad


-- 受け取った整数(Int型)に対し、4で割り切れたらBool型のTrueを、
-- 割りきれなかったら受け取った整数(型はIntでないかも)そのものを返す関数
isModBy4 = (id &&& flip mod 4) >>> first (repeat . Right)
            >>> first ((:) $ Left True) >>> uncurry (!!) >>> either (show) (show)

main = do print $ isModBy4 10  -- 4で割り切れないので10がそのまま返る
          print $ isModBy4 12  -- 4で割り切れるのでTrueが返る

{-
-- MonadとArrowの使用例

div2 :: Int -> Maybe Int
--div2 x = if even x then Just (x `div` 2)
--                   else Nothing
div2 x
 |even x = Just (x `div` 2)
 |otherwise = Nothing

--Pointwise
div8 x = (return x) >>= div2 >>= div2 >>= div2

--Pointfree
--div8 = \x -> (Just x) >>= div2 >>= div2 >>= div2
--div8 = runKleisli (Kleisli div2 >>> Kleisli div2 >>> Kleisli div2)

main::IO()
main = do print $ div8 32  -- 出力: Just 4
          print $ div8 50  -- 出力: Nothing
-}

